<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster&display=swap" rel="stylesheet"> 
    <title>Connexion</title>
</head>
<body>

<?php

include "navConnexion.html"

?>

<div id="formulaire">

    <p> Connexion </p>

    <form id="connexion" method="get" action="listeIntervention.php">
        <p>Identifiant</p> <input type="text" name="identifiant" placeholder="Saisir son identifiant"><br>
        <p>Mot de passe</p><input type="password" name="mdp" placeholder="Saisir son mot de passe"> <br>
        <div><input class="submit" type="submit" name="submit" id="envoyer" value="Connexion"></div>
    </form>

</div>

</body>
</html>